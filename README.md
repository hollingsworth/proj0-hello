# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

## Description: 
---------------
Program "hello.py" prints "Hello world", nothing more, nothing less
## Author:
---------------
Sydney Hollingsworth
sholling@uoregon.edu
